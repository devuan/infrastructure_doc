#!/bin/bash
#
# Helper script to kill an nbd-server (or anything found by pkill -f $1)
# when the proces of pid $2 terminates.
#

echo "Pending stop '$1' when '$2' terminates"
sleep 1
/usr/bin/tail -f /dev/null --pid $2
echo "Stopping all '$1'"
/usr/bin/pkill -f "$1"
exit 0
