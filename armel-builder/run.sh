#! /bin/bash
#
set -x
LOG=/tmp/$(basename $(readlink -f $0)).log
date >> $LOG
echo "$*" | sed "s/ -/\n -/g" >> $LOG

##
## Wrapper to call qemu-system-arm from Ganeti, removing all x86/PCI
## assumptions. This is for an armel host
##

# The setup for armel is complicated:
# The guest is: -machine versatilepb -cpu arm926 -m 256
# with NBD provided devices for rootfs and swap.
# rootfs, nbd0p1, is from the ganeti disk, and the swap, nbd1, is
# set up here at start as a 2G tmpfs.

# So, it involves running 3 separate daemons:
# nbd-server for providing the devices
# qemu-system-arm
# an nbd-reaper, that kills nbd-server when qemu terminates
#
# however, ganeti runs ths script in a new pid namespace, which plays
# havoc with the process control. Basically, this script ends up in
# zombie state after having spwned its children :(

# Set up the machine argument
args=( -machine versatilepb -cpu arm926 )

DISK=unknown
ID=$$
SWAP=/tmp/armel/swap
NBD=/tmp/NBD
NBDSRV=$(ip addr show lan_br| awk '$1=="inet"{print $2;exit}' | sed 's/\/.*//')
BASE=$(dirname $(readlink -f $0))
KERNEL=$BASE/zImage-4.19.0-versatile
INITRD=$BASE/initrd.img-4.19.0-versatile
DTB=$BASE/versatile-pb.dtb
NBDPORT=12000
REAPER=$(dirname $0)/nbd-reaper.sh

create_swap() {
    [ -e $SWAP ] && return 0
    mkdir -p $(dirname $SWAP)
    mount -t tmpfs -osize=2G none $(dirname $SWAP)
    dd if=/dev/zero of=$SWAP bs=10M 2>/dev/null || true
    mkswap $SWAP
}

start_net_disk() {
    for NBDPORT in {12000..12500} ; do
	X="$(nc -l -p $NBDPORT -w 1 2>&1)"
	echo "port $NBDPORT - '$X'" >> $LOG
	[ -z "$X" ] && continue
	[ -z "${X#*timed out}" ] && break
    done
    echo "Starting $DISK service on port $NBDPORT" >> $LOG
    # Prepare the swap device
    create_swap
    # prepare the NBD control script
    cat <<EOF > $NBD
[generic]
[rootfs]
  exportname = $1
  fua = true
  flush = true
[swap]
  exportname = $SWAP
  fua = true
  flush = true
EOF
    # start nbd-server
    daemon -- nbd-server ${NBDSRV}:${NBDPORT} -C $NBD > /dev/null
}

# Now, process the command line arguments into a new $args
while [ "$1" ]; do
    case "$1" in
	-machine|-M)
	    # Ignore the machine argument (it is already defined above),
	    # except when querying the list of machines
	    if [ "$2" = "?" ] ; then
		args+=( -M "?" )
	    fi
	    shift
	    ;;
	--help)
	    args=( --help )
	    break
	    ;;
	-usb)
	    : # skip any -usb argument
	    ;;
	-usbdevice|-cpu)
	    # skip any usbdevice and cpu arguments
	    shift
	    ;;
	-pidfile)
	    QEMUPID="$2"
	    args+=( -pidfile "$QEMUPID" )
	    shift
	    ;;
	-drive)
	    # Capture the drive argument as the disk
	    DISK=${2%%,*}
	    DISK=${DISK#file=}
	    start_net_disk $DISK
	    APPEND=(
		boot=armel-boot
		root=/dev/nbd0p1
		armel-boot=${NBDSRV}:${NBDPORT},192.168.97.119,eth0
		mem=256M console=ttyAMA0,115200 earlyprintk
	    )
	    args+=(
		-kernel $KERNEL -initrd $INITRD -dtb $DTB
		-append "${APPEND[*]}"
	    )
	    echo "Started nbd-server" >> $LOG
	    pgrep -a nbd >> $LOG
	    echo "********************" >> $LOG
	    shift
	    ;;
	-device)
	    # net guest devices, just use the default
	    # rely on order to make host association
	    MAC=nic,macaddr=${2#virtio,mac=}
	    NETDEV=${MAC#*,netdev=}
	    MAC=${MAC%,bus=*},netdev=$NETDEV
	    args+=( -net ${MAC} )
	    shift
	    ;;
	-smp)
	    # Revise the smp argument
	    args+=(
		-smp $2 # TBD ,maxcpus=4
	    )
	    shift;
	    ;;
	*)
	    # Replace PCI devices by their bus agnostic variant, and drop
	    # all the PCI related options
	    # remove any vhost settings
	    # remove the console= directive
	    X="$(echo "$1" | sed -e 's/virtio-\(\w\+\)-pci/virtio-\1-device/' -e 's/,bus=[^,]\+,addr=[^,]\+//')"
	    X="$(echo "$X" | sed -e 's/,vhost[^,]\+//g')"
	    X="$(echo "$X" | sed -e 's/console=[^ ]*//g')"
	    args+=( "$X" )
	    ;;
    esac
    shift
done

echo "================================" >> $LOG
echo "${args[@]}" | sed "s/ -/\n -/g" >> $LOG

export QEMU_AUDIO_DRV=alsa
# Run as sub process in order to stop the net_disk on exit
/usr/bin/qemu-system-arm "${args[@]}" || exit 1
[ -z "$QEMUPID" ] && exit 0

daemon -- $REAPER "nbd-server ${NBDSRV}:${NBDPORT} " $(cat $QEMUPID) >> $LOG

echo "End of run.sh" >> $LOG
