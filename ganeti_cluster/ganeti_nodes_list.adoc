Ganeti Nodes
============

[options="header",cols="^,^"]
|====
|host name| ipv4 address
| newtonia| 46.105.97.110 (master)
| napier | 37.59.56.43
| nash | 94.23.30.104
| nardoo | 37.187.145.27
|====

Restricted access
https://dev1galaxy.org/other/infra/vmgraph.html[hosting graph]

