#!/bin/bash
#
# Set up ipv4 default route based on least ping timing for localnet lan_br

GWS=( 192.168.97.{12..15} )

function pingtime() {
    local T I F
    T=$(ping -n -c 1 $1 | grep -oE "time=[^ ]*")
    I=${T%.*}
    I=000000${I#time=}
    I=${I:$((${#I}-4)):4}
    F=${T#*.}000000
    echo ${I}${F:0:6}-$1
}

X=( $(for i in ${GWS[@]} ; do pingtime $i ; done | sort -n) )
echo ${X[*]} | xargs -n1 echo
set -x
ip route replace default via ${X#*-} dev eth1
