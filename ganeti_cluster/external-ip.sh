#!/bin/bash
#
# Set up external IP routes and bypass wanbr

IPS=( 5.196.38.{16..31} )

for ip in ${IPS[@]} ; do
    ip route add $ip dev lan_br
    ebtables -t broute -A BROUTING -p IPv4 -i eth0 --ip-dst $ip \
	     -j redirect  --redirect-target DROP
done
