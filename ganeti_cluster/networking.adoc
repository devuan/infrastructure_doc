Devuan Infrastructure Networking
================================

This is a documentation note regarding the ipv6 setup on the Devuan
infrastructure. In general, the "ganeti nodes" are set up as
forwarding routers for VMs with ipv6 address allocations on localnet.
This is facilitated by using +ndppd+ for proxying the Neighbor
Discovery Protocol between the wide area network interface, +wanbr+,
and the internal, virtual local network interface, +lan_br+, which
spans across all nodes by means of
link:ganeti-hosts-VPNs.adoc[virtual cabling].

Each node has an ipv6 address in form xxx/64, i.e. an address block
with a 64 bit prefix, and their respective gateways are found at the
corresponding addresses of xxx/56 + ::ff:ff:ff:ff:ff.

For example: the node +newtonia+ has address block
+2001:41d0:2:d06e::/64+ and +2001:41d0:2:d0ff:ff:ff:ff:ff+ as its
uplink gateway. We thus assign both the anycast address
+2001:41d0:2:d06e::/64+ and a distinct address
+2001:41d0:2:d06e::1/64+ to its +wanbr+ interface, and a distinct
address +2001:41d0:2:d06e::2/96+ to its localnet interface (+lan_br+),
to be used by VMs on that network.

A VM on that network gets its ipv6 address using its ipv4 address as
the last 32 bits. Thus, +galaxy+ which has ipv4 address +5.196.38.19+
(which is +05:c4:26:13+ in hex) is set up with the ipv6 address
+2001:41d0:2:d06e::5c4:2613/96+ and with +2001:41d0:2:d06e::2+ as its
default gateway.

.node ipv6 assignments
[options="header",cols="1,^3,^3,^3"]
|===========================
|node | wanbr | gateway | lan_br
|napier
 | 2001:41d0:8:732b::/64
 2001:41d0:8:732b::1/64
 | 2001:41d0:8:73ff:ff:ff:ff:ff
 | 2001:41d0:8:732b::2/96
|nardoo
 | 2001:41d0:a:511b::/64
 2001:41d0:a:511b::1/64
 | 2001:41d0:a:51ff:ff:ff:ff:ff
 | 2001:41d0:a:511b::2/96
|nash
 | 2001:41d0:2:1f68::/64
 2001:41d0:2:1f68::1/64
 | 2001:41d0:2:1fff:ff:ff:ff:ff
 | 2001:41d0:2:1f68::2/96
|newtonia
 | 2001:41d0:2:d06e::/64
 2001:41d0:2:d06e::1/64
 | 2001:41d0:2:d0ff:ff:ff:ff:ff
 | 2001:41d0:2:d063::2/96
|===========================

Note that the +lan_br+ configuration installs an automatic route for
+xxx/96+ onto that interface, which means that VMs will be reachable
without further node configuration, except that

 a. its ipv6 forwarding must be enabled
 (+net.ipv6.conf.all.forwarding=1+),

 b. the Neighbor Discovery Protocol on +wanbr+ must be proxied onto
    +lan_br+. The nodes run +ndppd+ for that.


Example Command Sequences
-------------------------

Command sequences for setup of nodes and VMs are as follows:

.example node: newtonia
----
# ip -6 addr add 2001:41d0:2:d06e::/64 dev wanbr
# ip -6 addr add 2001:41d0:2:d06e::1/64 dev wanbr
# ip -6 route add 2001:41d0:2:d0ff:ff:ff:ff:ff dev wanbr
# ip -6 route add default via 2001:41d0:2:d0ff:ff:ff:ff:ff dev wanbr

# ip -6 addr add 2001:41d0:2:d06e::2/96 dev lan_br

# sysctl net.ipv6.conf.all.forwarding=1

# apt-get install ndppd
# cat << EOF > /etc/ndppd.conf
proxy wanbr {
   rule 2001:41d0:2:d06e::/96 {
       iface lan_br
   }
}
EOF
# service ndppd restart
----

.example VM: galaxy
----
# ip -6 addr add 2001:41d0:2:d06e::3624:8eb1/96 dev eth1
# ip -6 route add default via 2001:41d0:2:d06e::2 dev eth1
----


The Persistent Configuration
----------------------------

This table presents the persistent configuration of the four nodes. It
shows the relevant snippets in +/etc/network/interfaces+,
+/etc/systl.conf+ and +/etc/ndppd.conf+.

[cols="1,10,8,8"]
|====
| node | /etc/network/interfaces | /etc/sysctl.conf | /etc/ndppd.conf
a|
newtonia
a|
----
iface wanbr inet static
    address 46.105.97.110/24
    gateway 46.105.97.254
    up ip addr add 2001:41d0:0002:d06e::/64 dev wanbr
    up ip addr add 2001:41d0:0002:d06e::1/64 dev wanbr
    up ip route add 2001:41d0:0002:d0ff:ff:ff:ff:ff dev wanbr
    up ip route add ::/0 via 2001:41d0:0002:d0ff:ff:ff:ff:ff dev wanbr

iface lan_br inet static
    up ip addr add 2001:41d0:0002:d06e::2/96 dev lan_br
----
a|
----
net.ipv6.conf.all.forwarding=1
----
a|
----
proxy wanbr {
   rule 2001:41d0:2:d06e::/96 {
       iface lan_br
   }
}
-----
a|
nash
a|
----
iface wanbr inet static
    address 94.23.30.104/24
    gateway 94.23.30.254
    up ip addr add 2001:41d0:2:1f68::/64 dev wanbr
    up ip addr add 2001:41d0:2:1f68::1/64 dev wanbr
    up ip route add 2001:41d0:2:1fff:ff:ff:ff:ff dev wanbr
    up ip route add ::/0 via 2001:41d0:2:1fff:ff:ff:ff:ff dev wanbr

iface lan_br inet static
    up ip addr add 2001:41d0:2:1f68::2/96 dev lan_br
----
a|
----
net.ipv6.conf.all.forwarding=1
----
a|
----
proxy wanbr {
   rule 2001:41d0:2:1f68::/96 {
       iface lan_br
   }
}
-----
a|
napier
a|
----
iface wanbr inet static
    address 37.59.56.43/24
    gateway 37.59.56.254
    up ip addr add 2001:41d0:0008:732b::/64 dev wanbr
    up ip addr add 2001:41d0:0008:732b::1/64 dev wanbr
    up ip route add 2001:41d0:0008:73ff:ff:ff:ff:ff dev wanbr
    up ip route add ::/0 via 2001:41d0:0008:73ff:ff:ff:ff:ff dev wanbr

iface lan_br inet static
    up ip addr add 2001:41d0:0008:732b::2/96 dev lan_br
----
a|
----
net.ipv6.conf.all.forwarding=1
----
a|
----
proxy wanbr {
   rule 2001:41d0:8:732b::/96 {
       iface lan_br
   }
}
-----
a|
nardoo
a|
----
iface wanbr inet static
    address 37.187.145.27/24
    gateway 37.187.145.254
    up ip addr add 2001:41d0:a:511b::/64 dev wanbr
    up ip addr add 2001:41d0:a:511b::1/64 dev wanbr
    up ip route add 2001:41d0:a:51ff:ff:ff:ff:ff dev wanbr
    up ip route add ::/0 via 2001:41d0:a:51ff:ff:ff:ff:ff dev wanbr

iface lan_br inet static
    up ip addr add 2001:41d0:a:511b::2/96 dev lan_br
----
a|
----
net.ipv6.conf.all.forwarding=1
----
a|
----
proxy wanbr {
   rule 2001:41d0:a:511b::/96 {
       iface lan_br
   }
}
-----
|====
