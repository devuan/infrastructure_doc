About the ganeti hosts VPN(s)
=============================

The ganeti hosts' bridge interfaces +gnt_br+ and +lan_br+ are
inter-connected respectively at Ethernet (Level 2) level by means of
+rrqnet+ "virtual cables". This cabling extends the local networks to
be a pair of shared networks across all the hosts. See
[devuan/rrqnet](https://git.devuan.org/devuan/rrqnet) (formerly
+udptap+) for details about the software involved.

The cabling setup at 2020-10-06 is as follows:

The virtual cabling has been upgraded to use fully connected mesh for
+ganetinet+ interconnecting the +gnt_br+ bridges, and for +localnet+
interconnecting the +lan_br+ bridges. In this setup, each of the nodes
run two +rrqnet+ plugs that connect to the corresponding +rrqnet+
plugs on the other nodes; one such for +ganetinet+ and the other for
+localnet+. Example set up for +nash+:

----
auto ganetinet
iface ganetinet inet manual
    rrqnet_port 2001
    rrqnet_options -4 -B 10 -T 5
    rrqnet_remote 46.105.97.110:2001
    rrqnet_remote 37.59.56.43:2001
    rrqnet_remote 37.187.145.27:2001
    rrqnet_bridge gnt_br

auto localnet
iface localnet inet manual
    rrqnet_port 2002
    rrqnet_options -4 -B 10 -T 5
    rrqnet_remote 46.105.97.110:2002
    rrqnet_remote 37.59.56.43:2002
    rrqnet_remote 37.187.145.27:2002
    rrqnet_bridge lan_br
    rrqnet_log -v /tmp/localnet.log
----

As a result, each pair of nodes is at a one-hop tunneling distance. It
is then important that the bridges on all nodes have their STP logic
turned off as it otherwise would distort the connectivity into a
spanning tree.

All nodes have the same set up except for their +rrq_remote+
collections which must not include the node itself.

External IP Tunneling
---------------------

External IP is tunneled through localnet. The incoming node has a
routing rule to forward the external IP packets onto localnet
(+lan_br+):
----
ip route add $EXTIP dev lan_br
----

Note that OVH assigns the Etherenet address for the external IP, and
therefore there is no ARP proxing required.

The current +wanbr+ bridges are "bypassed" for externa IP by means of
+ebtables+ rules like the following:
----
ebtables -t broute -A BROUTING -p IPv4 -i eth0 --ip-dst $EXTIP -j redirect --redirect-target DROP
----

Each targeted VM is set up with a secondary address on their +eth1+,
and a source based routing for return packets. The snippet in their
+/etc/network/interfaces+ is like the following:
----
auto eth1:ext
iface eth1:ext inet static
    hwaddress $EXTMAC
    address $EXTIP/32
    up ip rule add from $EXTIP lookup external
    up ip route add default via $GW dev eth1:ext table external
    down ip route del default via $GW dev eth1:ext table external
    down ip rule del from $EXTIP lookup external
----

In that snippet, +$GW+ is the localnet IP for the node that receives
packets for the external IP, and +$EXTMAC+ is the targeted Ethernet
address as assigned by OVH.

The +external+ routing tables are manually set up in the usual way:
----
echo 201 external >> /etc/iproute2/rt_tables
----

Further, a script +/etc/network/bestlocalnet.sh+ was prepared in
support of VM migration, and that script is set up run when the
localnet interface, +eth1+, is brought up so as to set the default
route to the nearest +lan_br+. Thus, external IP traffic is sent via
the +lan_br+ of the node that receives that IP traffic, and all other
traffic is sent via the nearest +lan_br+.

./etc/network/bestlocalnet.sh
----
#!/bin/bash
#
# Set up ipv4 default route based on least ping timing for localnet lan_br

GWS=( 192.168.97.{12..15} )

function pingtime() {
    local T I F
    T=$(ping -n -c 1 $1 | grep -oE "time=[^ ]*")
    [ -z "$T" ] && return 0
    I=${T%.*}
    I=000000${I#time=}
    I=${I:$((${#I}-4)):4}
    F=${T#*.}000000
    echo ${I}${F:0:6}-$1
}

X=( $(for i in ${GWS[@]} ; do pingtime $i ; done | sort -n) )
echo ${X[*]} | xargs -n1 echo
set -x
ip route replace default via ${X#*-} dev eth1
----
