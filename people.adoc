Devuan Operation Roles
======================

This page is a summary of the Devuan (sub) systems for the purpose of
documenting who are involved in keeping them running. The various
(sub) systems have technical descriptions elsewhere; here they are
only referred to in name.

Obviously some (sub) systems involve both a "public facing" role aimed
at representing Devuan in interactions with (sub) system "users" and a
"technical administration" role aimed at making and keeping the (sub)
system available. Those roles may be held differently, by different
people, and some of those cases are indicated in the table below.

The table does not represent any kind of "ownership" of the (sub)
systems; all "policy making" aspects are strategic rather than
operational and thus left to be understood separately. All (sub)
systems belong to the Devuan organisation as a whole.

This write-up only concerns people's operational roles.

Edited: 2021-03-28 (rrq)

.shop front systems
[cols="2,5",options="header",width="100%"]
|===
|(sub) system
|People

| www.devuan.org
| Xenguy (content), plasma41 (content/tech)

| dev1galaxy.org
| golinux (admin), fsmithred (admin), rrq (tech)

| package building documentation
| leepen

| infrastructure documentation
| rrq, bbonev

| wiki.devuan.org
| hendrick, plentyn

| IRC channel(s)
| parazyd

| ZNC IRC proxy
| rrq

| mail groups
a|[frame="none",cols="2,4",width="100%"]
!====
! dng@lists.dyne.org ! golinux, jaromil, adam
! devuan-dev@lists.dyne.org ! golinux, leepen
! freedom@devuan.org ! golinux, jaromil
!====

| keyring.devuan.org
| rrq

| pkginfo.devuan.org
| rrq

| bugs.devuan.org
| leepen

| popcon.devuan.org
| leepen

| files.devuan.org
a|fsmithred
[frame="none",cols="2,5",width="100%"]
!====
! live installer ISO ! fsmithred
! plain installer ISO ! rrq
!====

| arm-files.devuan.org
a|fsmithred
[frame="none",cols="2,5",width="100%"]
!====
! arm images ! tuxd3v, adam
!====

| git.devuan.org, aka git store or gitea
| leepen, bbonev

| pkgmaster.devuan.org
| leepen

| apt-panopticon
| onefang

| devuan.pro
| jaromil
|===


.back-office systems
[cols="2,5",options="header,pgwide",width="100%"]
|===
|(sub) system
|People

| anextrawidecelltomaketablewide
| and this is empty on purpose

| beta web site
| Xenguy (content), plasma41 (content/tech)

| desktop theme
| vacant

| forum
a|[frame="none",cols="2,5",width="100%"]
!====
! fluxbb ! rrq
! stopforumspam ! rrq
! languages ! rrq (tech), /contributors/
! themes ! golinux, rrq
! /files ! rrq
! /other ! rrq
!====

| git store
a|[frame="none",cols="2,5",width="100%"]
!====
! gitea ! bbonev
! spammer hammer ! bgstack15
!====

| amprolla
| leepen, bbonev

| repository mirroring
| onefang

| backup
| leepen, rrq

| mailer
| amesser

| jenkins
| leepen, bbonev

| dak
| leepen, bbonev

| britney
| leepen, bbonev

|===

.infrastructure systems
[cols="2,5",options="header",width="100%"]
|===
|(sub) system
|People

| SoYouStart
| jaromil, rrq, leepen, centurion_dan, nextime

| Domain Names
a|[frame="none",cols="2,5",width="100%"]
!====
! dev1.cloud ! evilham
! dev1galaxy.org ! jaromil
! devuan.dev ! golinux
! devuan.info ! golinux
! devuan.net ! jaromil
! devuan.nz ! centurion_dan
! devuan.pro ! jaromil
! devuan.org ! nextime
!====

| MTA
a|[frame="none",cols="2,5",width="100%"]
!====
! @devuan.org ! (Dyne) jaromil, amesser
! @devuan.dev ! leepen, rrq
! @lists.dyne.org ! (Dyne) jaromil, adam, rick
!====

| ipv4/ipv6 allocations, routing and rDNS
| bbonev, rrq

| firewall, baddie stamping
| bbonev, rrq

| DNS service
| bbonev

| deb.roundr.devuan.org
| bbonev

| DNS cache
| bbonev, rrq

| NTP
| bbonev, rrq, leepen

| virtual cabling
| bbonev, rrq

| ganeti
| bbonev, rrq, leepen

| ssh users on nodes and VMs
| bbonev

| the nodes
|bbonev, rrq, leepen

| associated hosts
a|[frame="none",cols="2,5",width="100%"]
!====
! build017-talosII-ppc64el ! centuron_dan
! deb.devuan.nz ! centurion_dan
! mini.rrq.dev1net ! rrq, leepen (~devuan)
! native_arm64_builder ! parazyd
! sledjhamr.org ! onefang
! ns3 / borta.rrq.id.au ! rrq
! ns4 ! bbonev
! ns5 ! bbonev
! ns6 ! bbonev
!====

// end of infrastructure table
|===
