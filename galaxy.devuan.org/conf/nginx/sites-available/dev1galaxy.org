# www.dev1galaxy.org --> dev1galaxy.org
server {
    listen 80;
    listen 443 ssl;
    server_name www.dev1galaxy.org galaxy.devuan.org;
    include snippets/ssl-dev1galaxy.org.conf;
    include snippets/ssl-params.conf;

    return 301 $scheme://dev1galaxy.org$request_uri;
}

# dev1galaxy.org / galaxy.devuan.org
server {
    listen 80;
    listen [::]:80;
    listen 443 ssl;
    listen [::]:443 ssl;

    root /home/flux/forum;
    index index.php index.html index.htm;

    server_name dev1galaxy.org;
    # server_name dev1galaxy.org galaxy.devuan.org;
    include snippets/ssl-dev1galaxy.org.conf;
    include snippets/ssl-params.conf;

    location / {
        try_files $uri $uri/ /index.html;
    }

    error_page 404 /404.html;

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    # pass the PHP scripts php5_flux upstream
    # (see ../conf.d/upstream_php5_flux.conf)
    location ~ \.php$ {
        try_files $uri =404;
        
        fastcgi_pass php5_flux;

        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /.well-known {
        allow all;
    }
}

