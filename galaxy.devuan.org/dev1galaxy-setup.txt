In a nutshell, d1g needs:
  - php5(-fpm) to run
  - postgresql for the db
  - a flux user that hosts the db and runs the php code
  - email setup to be able to send emails to users
  - cron jobs for the different bots
  - firewall for improved security

Quick overview of what to do:

* install iptables-persistent for firewalling
  ensure it's early in boot.
  currently block all incoming but tcp:22,
  should open tcp:80 and tcp:443 later.

* copy this note, and start an /root/done.txt file

* install ntpdate, and set up cron line
  */10 * * * * /usr/sbin/ntpdate ??.pool.ntp.org > /dev/null

* install emacs24-nox iselect and add the edapt script /usr/local/bin
  (those are for me)

* add a flux user for owning the forum database

* install nginx postgresql php5-cli php5 postfix letsencrypt

  + set up nginx for fluxbb .. virtual hosting?
    (old dev1galaxy does not have virtual hosting)
    all files owned by flux, accessable by nginx

  + set up postgres with the flux user's database named flux

  + set up php5-fpm as required for nginx (config files attached)

  + set up postfix for email sendout from fluxbb.
    maybe "smarthost" set up via dyne? (didn't work before..)

  + set up letsencrypt. The domain remains as dev1galaxy.org
    might need a test cert for galaxy.devuan.org set up as well?
    or we do http through ssh redirection for testing?

* add forum software and content and gdolink bot
  + the bot is still turned off (bug in issues loading)

* set up ralph's "snaplock" (pam script & nginx log)
  install ipset pam-script
  + comment out all except in /etc/pam.d/common-auth
  + add /usr/share/libpam-script/capture_pam_dubious with a link from
    /usr/share/libpam-script/pam_script_auth
    capture_pam_dubious essentially does:
    /sbin/ipset add DUBIOUS "[${PAM_RHOST}]"
    but avoid certain cases.

+ add cron line for capture_nginx_dubious
* verify it's working

* change DNS to point to new server.
  + both dev1galaxy.org and m.dev1galaxy.org
  + update the SPF record ?
