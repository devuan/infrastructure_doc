About ganeti build hosts
========================
:doctype: article
:Author: Ralph Ronnquist
:revdate: {sys:date "+%Y-%m-%d %H:%M:%S"}

Introduction
------------

Devuan's package building pipeline includes five virtual machines (VM)
on the ganeti platform for building packages on different machine
architectures. These are:

  * +amd64-builder+, for +amd64+ building, which is the native
    architecture.
  * +i386-builder+, for +i386+ building through +qemu-system-i386+
    with +kvm+ emulation.
  * +arm64-builder+, for +arm64+ building on a fully emulated
    +qemu-system-aarch64+ host using the +virt+ model.
  * +armhf-builder+, for +armhf+ building on a fully emulated
    +qemu-system-arm+ host using the +virt+ model.
  * +armel-builder+, for +armel+ building on a fully emulated host
    +qemu-system-arm+ host using the +versatilepb+ model.

Note: as of 2020-06-22 only +amd64-builder+ and +i386-builder+ are in
service but the +arm*-builder+ descriptions are preserved as
historical documentation.

The key to running different emulations on +ganeti+ is in the set up
of the +kvm_path+ attribute of the instance. The default emulator is
set as +/usr/bin/kvm+, which is a script invoking
+qemu-system-x86_64+. The other instances are given their distinct
emulator control scripts with transparently different emulator
choices.

The build hosts are set up with their individual administration
directories under +/root+:

  * +/root/amd64-builder+
  * +/root/i386-builder+
  * +/root/arm64-beowulf+
  * +/root/armhf-builder+
  * +/root/armel-builder+

The +arm+ architectures deploy their +kernel+ and +initrd+ from their
administration directories, while +amd64+ and +i386+ deploy their
+kernel+ and +initrd+ from the +/boot+ directory on their primary
disks.

These VMs are managed as +ganeti+ instances as per normal,
except for +armel-builder+, which has additional support of a
dedicated Network Block Device (NBD) server.

About amd64-builder
-------------------

This is a ganeti VM with 4 Gb RAM and 8 Gb disk. It has two network
interfaces linked to the ganeti network and the localnet network.

About i386-builder
------------------

This is a ganeti VM with 4 Gb RAM and 8 Gb disk. It has two network
interfaces linked to the ganeti network and the localnet network. It
has +/usr/bin/kvm-i386+ as +kvm_path+.

About arm64-beowulf
-------------------

Note that this directory was accidentally mis-named.

This is a ganeti VM with 4 Gb RAM and 8 Gb disk. It has two network
interfaces linked to the ganeti network and the localnet network. It
has +/usr/bin/kvm-aarch64+ as +kvm_path+, and its kernel (+vmlinuz+)
and initrd (+initrd.img+) are in the administration directory.


About armhf-builder
-------------------

This is a ganeti VM with 4 Gb RAM and 8 Gb disk. It has two network
interfaces linked to the ganeti network and the localnet network. It
has +/usr/bin/kvm-armhf+ as +kvm_path+, and its kernel (+vmlinuz+) and
initrd (+initrd.img+) are in the administration directory.


About armel-builder
-------------------

This VM has a more involved set up. See
link:armel-builder/armel-builder.adoc["About armel-builder"] for
details.
