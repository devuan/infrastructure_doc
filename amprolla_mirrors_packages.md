Current status
==============

packages.devuan.org
-------------------

* packages.devuan.org is our main mirror
        * dak pushes .deb and Packages files here to /devuan
        * amprolla pushes Packages and other files here to /merged

amprolla
--------

* https://git.devuan.org/devuan-infrastructure/amprolla
        * this is the running version
        * located on a separated machine accessible through VPN
        * takes packages.devuan.org/devuan + deb.debian.org and combines
                * ships those back to packages.devuan.org/merged

dak
---

* please fill in

amprolla3
---------

* https://git.devuan.org/maemo/amprolla
        * this is the reimplementation of the initial amprolla done by
          Wizzup and parazyd
* For Devuan, it has been deployed on a VM on the Devuan infrastructure
* Disk usage:
        * spool dir (unmerged devuan + debian-security + debian)
                * 2.2G
        * merged dir
                * 392M (without Contents files)
                * ~2.2G (with Contents files)
                * multiply by 3 if we are using 3-dir rotation
                        * this needs evaluation if necessary or not
                        * can be evaluated when in production

pkgmaster.devuan.org
--------------------

* A machine planning to be the main mirror where all other mirrors will
  pull from.
* has been set up by KatolaZ, so he's the person to give more info.
* (KatolaZ) More info are inside the folder "pkgmaster". Sparse details below:

  * has a shell account accessible by the amprolla machine where amprolla
    rsyncs the merged files after every merge/update
  * has a restricted shell account allowing only rsync pull
    (this access is given to mirrors)

TODO/Wishlist
-------------

* [dak] Fix the Release headers in dak to represent valid ones
        * drawkula and parazyd can explain this once the task is active
        * a hack has been implemented in amprolla3 to mitigate this, but for
          a valid solution - it should be fixed upstream - dak
* [general] Proceed with moving the "Future status" to current production



Future status
=============

packages.devuan.org
-------------------

* I am not sure what will happen with packages.devuan.org in the future
        * we will have to evaluate when the stuff below reaches production
        * or if someone has unspoken plans, please leave them here


TODO/Wishlist
--------------

* Nothing ATM
