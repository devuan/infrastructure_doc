About the DNS set up
====================
:doctype: article
:Author: rrq
:Revision: 0.2

These are some notes about the Devuan domain names. Base domains in
use include:

* dev-1.org
* dev-one.org
* dev1galaxy.org
* devuan.dev
* devuan.org
* devuan.net
* devuan.info

== Domains

=== Domain: dev-one.org and dev-1.org

These domains are available to Devuan, although not in use at the moment.
They are owned by `jaromil` (Dyne) and administered by `jaromil`.
Hosts of this domain include:

  - www.dev-one.org - Devuan web site (alternative names)

=== Domain: dev1galaxy.org

This is the official domain dedicated for the Devuan Forum. It is owned by
`jaromil` (Dyne.org foundation) and administered by `jaromil`.
Hosts of this domain include:

  - dev1galaxy.org - Devuan forum

=== Domain: devuan.dev

This is phased in as base domain for Devuan development resources. It
is owned by `golinux`, and administered with the Devuan hosted DNS sub
system, described below. Hosts of this domain include:

  - gitea.devuan.dev - Devuan git repository (2nd generation)

=== Domain: devuan.org

This is the initial official base domain for Devuan services. It is
owned by `nextime` (Unixmedia s.r.l.) and administered with the Devuan
hosted DNS sub system. Hosts of this domain include:

  - www.devuan.org - Devuan web site (initial hosting)
  - files.devuan.org - Devuan ISO downloads
  - packages.devuan.org - Devuan package repository
  - ci.devuan.org - Devuan package builder
  - amprolla.devuan.org - Devuan distribution builder
  - git.devuan.org - Devuan git repositories (1st generation)
  - bugs.devuan.org - Devuan bug tracker
  - popcon.devuan.org - Devuan popcon server

=== Domain: devuan.net

Owned by `jaromil` (Dyne) and administered on Devuan's DNS infra.
Redirects @ and www to devuan.org

=== Domain: devuan.info

Owned by `golinux`, not configured.

== The Devuan hosted DNS

The Devuan Domain Name Servers is a distributed set up using
https://en.wikipedia.org/wiki/NSD[NSD], with a "private" primary server
(`ns1`) and several public secondaries (`ns2` to `ns6`). These provide
authoritative resolutions for `devuan.dev`, `devuan.org` and `devuan.net`.

The servers `ns1` and `ns2` are virtual hosts on Devuan's ganeti
platform, and they interact over the local `dev1nsd_br` bridge.
The bridge setup uses vlan 1000 over the `rtn_br` which connects all
hosts with full mesh tunneling and split horizon bridging to allow
always using the best IP route path between the hosts.

The server `ns3` is `rrq's` VPS in Sydney, AU, with a rrqnet VPN via `nash`
named `dev1nsd` and member in `dev1nsd_br` to reach `ns1` for distributing
the domain set up.

See `ns1:/etc/hosts` for IP addresses.

The other secondaries - `ns4`, `ns5` and `ns6` are provided by `bbonev` and
connected to the `rtn_br` on all hosts via `gretap` tunnels and use vlan 1000
for the 10.0.10.0/24 subnet to communicate with `ns1`.

They have the following details:

- ns4 - Sofia, BG; IPv4 195.85.215.108, IPv6 2a01:9e40::108
- ns5 - Mathura, IN; IPv4 103.146.168.108
- ns6 - Saint Petersburg, RU; IPv4 193.36.35.123
