#!/bin/sh

##
## Send the list of configured mirrors to the 
##
##   devuan-mirrors@lists.dyne.org
##
## mailing list
##

DATETIME=$(date +"%A %d %b %Y")
LIST_MIRRORS=~/script/list_mirrors.sh
MIRROR_STATUS=~/script/check_mirrors.sh
TMPFILE=$(tempfile)
MAIL_SUBJECT="Devuan Package Mirrors -- weekly update"
LIST_ADDR="devuan-mirrors@lists.dyne.org"

cat <<EOF >>$TMPFILE
Dear Mirror Admins,

Please find below the full list of Devuan package mirrors as
of $DATETIME:

---- BEGIN MIRROR-LIST ----
EOF

${LIST_MIRRORS} | uniq >> $TMPFILE

cat <<EOF >>$TMPFILE
---- END MIRROR-LIST ----
	
Please contact "mirrors@devuan.org" if any of the information 
above needs to be amended. 

Please see below the current status of the Devuan Package Mirror
network:

---- BEGIN MIRROR-STATUS ----
EOF

## exec check_mirrors.sh and strip escape characters from the output
${MIRROR_STATUS} 2>&1 | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" >>$TMPFILE 

cat <<EOF >>$TMPFILE
---- END MIRROR-STATUS ----

Thanks for your precious help in ensuring that Devuan GNU+Linux 
remains a universal, stable, dependable, free operating system.

Love

The Dev1Devs
EOF

mutt -s "${MAIL_SUBJECT}" ${LIST_ADDR} < $TMPFILE

rm $TMPFILE

