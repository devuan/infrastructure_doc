#!/bin/sh

MIRROR_FILE=~/mirror_list.txt

if [ $# -gt 0 ]; then 
	MIRROR_FILE=$1
fi

SHOWKEYS=" FQDN BaseURL Bandwidth Rate Active Country "

while IFS=':' read header value; do

	#echo "DEBUG - header: $header"
	#echo "DEBUG - value: $value"
	if [ -n "$header" ]; then 	
		#echo "DEBUG - header matched"
		if [ $(echo $SHOWKEYS | grep -c "$header" ) != "0" -a -n "$value" ]; then 
			echo "$header: $value"
		fi	
	fi
	if [ -z "$header" -a -z "$value" ]; then 
		echo
	fi
done < ${MIRROR_FILE}

