#!/bin/sh

#
# Check the rewrite rules for all the mirrors in the DNS Round-Robin
#
#

TEST_MIRROR=~/script/test_rewrite.sh
#DISABLE_UPDATED="true"
#DISABLE_INTEGRITY="true"
CHECK_UPDATED=~/script/check_update_date.sh
CHECK_INTEGRITY=~/script/check_integrity.sh
RR_URL="deb.devuan.org"


if [ $# -gt 0 ]; then 
	MIRROR_FILE=$1
	echo "Setting MIRROR_FILE=${MIRROR_FILE}" 1>&2
fi


MIRRORS=$(host deb.devuan.org | awk '{print $4'} | sort -n)

for m in $MIRRORS; do
	printf "$m...." 1>&2
	
	## check deb.devuan.org
	printf " DNS-RR: " 1>&2
	m_base=$(echo $m | cut -d "/" -f 1)
	##echo "RR-URL: ${RR_URL}"
	${TEST_MIRROR} http://${m_base} ${RR_URL} >/dev/null
	ret=$?
	if [ $ret -eq 0 ]; then 
		printf "[\033[32mOK\033[0m]" 1>&2
		DNSRR_TEST="PASSED"
	else
		printf "[\033[31mFAILED\033[0m] ($ret errors)" 1>&2
		FAULTY="${FAULTY}\n$m_base (DNS-RR)"
		DNSRR_TEST="FAILED"
	fi

	## check mirror integrity
	printf " Integrity: " 1>&2
	if [ -z "${DISABLE_INTEGRITY}" -a "${DNSRR_TEST}" = "PASSED" ]; then 
		${CHECK_INTEGRITY} http://${m_base} ${RR_URL} >/dev/null
		ret=$?
		if [ $ret -eq 0 ]; then 
			printf "[\033[32mOK\033[0m]" 1>&2
		else
			printf "[\033[31mFAILED\033[0m] ($ret errors)" 1>&2
			FAULTY="${FAULTY}\n$m_base (Integrity)"
	 	fi
	else
			printf "[\033[33mSKIP\033[0m]" 1>&2
	fi
	
	## check mirror update
	printf " Updated: " 1>&2
	if [ -z "${DISABLE_UPDATED}" -a "${DNSRR_TEST}" = "PASSED" ]; then 
		${CHECK_UPDATED} http://${m_base} ${RR_URL} 2>/dev/null 
	else 
			printf "[\033[33mSKIP\033[0m]" 1>&2
	fi

	#echo 1>&2
done

echo "==== faulty mirrors: ====" 1>&2

[ -n "$FAULTY" ] && echo "$FAULTY" 1>&2 && exit 1
echo "[\033[32mOK\033[0m] No faulty mirrors" 1>&2 && exit 0

