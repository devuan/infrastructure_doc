#!/bin/sh


##
##
## Check that a list of users from certain hosts have connected through 
## ssh in the last INTERVAL number of minutes
##
## This is used to detect amprolla/packages.devuan.org not pushing
##
##


MYHOST="pkgmaster.devuan.org"
EMAILSUBJ="Error in rsync push on ${MYHOST}"


##
## checks to be performed, in the format:
##
##  CHKFILE:CHKUSER:CHKIP:CHKINT
##
CHKJOBS="/var/log/auth.log:mirror:46.105.97.110:amprolla:3600:katolaz@freaknet.org,parazyd@dyne.org\
	 /var/log/auth.log:mirror:46.105.191.77:packages:3600:katolaz@freaknet.org
"


TMPFILE=$(tempfile)

for j in $CHKJOBS; do 

	CHKFILE=$(echo $j | cut -d ":" -f 1)
	CHKUSER=$(echo $j | cut -d ":" -f 2)
	CHKIP=$(echo $j | cut -d ":" -f 3)
	CHKNAME=$(echo $j | cut -d ":" -f 4)
	CHKINT=$(echo $j | cut -d ":" -f 5)

	cat $CHKFILE | grep -a -E "sshd\[[0-9]+\]: Accepted publickey for $CHKUSER from $CHKIP" | tail -1 > $TMPFILE
	#cat ${TMPFILE}

	DATE1=$(date -d "$(cat ${TMPFILE} | cut -c-15)" +%s)
	##DATE2=$(date -d "$(tail -1 ${TMPFILE} | cut -d " " -f 1-3)" +%s)
	DATE2=$(date +%s)

	DATEDIFF=$(($DATE2 - $DATE1))
	echo $DATEDIFF
	if [ $DATEDIFF -gt $CHKINT ]; then
		ADDRESSES=$(echo $j | cut -d ":" -f 6 | sed -r -e 's/,/\ /g')
		for addr in $ADDRESSES; do 
			cat <<EOF | mutt  -s "${EMAILSUBJ}" ${addr}
error --  ${CHKFILE}: $CHKUSER@$CHKIP ($CHKNAME) has not logged on
$MYHOST for $DATEDIFF seconds (threshold at $CHKINT)

Please check the corresponding rsync-push job
EOF

		done	
	fi
done

rm $TMPFILE
