#!/bin/bash
set -e

# configurable part

MIRROR=http://pkgmaster.devuan.org/merged/
DIST=chimaera
CHROOT=/chroot
ARCH=arm64

# end

# some basic tools (separate lines for better diff)
PKG=psmisc
PKG+=,make
PKG+=,gcc
PKG+=,login
PKG+=,passwd
PKG+=,less
PKG+=,vim
PKG+=,netbase
PKG+=,wget
PKG+=,cpio
PKG+=,binutils
PKG+=,dpkg
PKG+=,apt
PKG+=,apt-utils
# deps not properly pulled by jenkins-debian-glue-buildenv-devuan (debootstrap resolver's fault)
PKG+=,perl-openssl-defaults
PKG+=,svn2cl
# jenkins builder glue
PKG+=,jenkins-debian-glue-buildenv-devuan
# missing deps from the above
PKG+=,openssh-client
PKG+=,default-jre-headless

if ! JUID=$(id -u jenkins 2>/dev/null); then
	echo First add the jenkins user on the host
	exit 1
fi

if [ -f $CHROOT/$ARCH ] || [ -d $CHROOT/$ARCH ]; then
	echo Cowardly refusing to overwrite $CHROOT/$ARCH
	exit 1
fi

mkdir -p $CHROOT/$ARCH

debootstrap --include="$PKG" --arch $ARCH $DIST $CHROOT/$ARCH $MIRROR

chroot $CHROOT/$ARCH adduser --gecos '' --disabled-password --uid $JUID jenkins

sed -i 's/127[.]0[.]0[.]1.*/127.0.0.1\tlocalhost '$(hostname)'/' $CHROOT/$ARCH/etc/hosts

