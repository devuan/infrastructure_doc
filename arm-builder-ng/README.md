Installing an ARM build host using chroot
=========================================

It is always preferable to isolate the build environment. The complete isolation
is best done by running it inside a VM but that infers some perfomance loss. Another
way is to run the builder in a chroot that offers no performance loss and some good
isolation.

The chroot method also allows to use a non-Devuan host but in that case the supplied
scripts do not support creating the chroot. One option is to create the chroot on a
Devuan host and copy it to the non-Devuan host.

Jenkins glue scripts (`jenkins-debian-glue-buildenv-devuan`) for builders support
building compatible architectures in higher capability hosts, e.g. `armel`/`armhf`
on `arm64` and `i386` on `amd64`.

These instructions document how to configure a builder in a chroot.

There are some hardcoded assumptions:
* chroot path is `/chroot/$ARCH` (this can easily be changed)
* architecture is `arm64` (same applies for amd64)

1) Add `jenkins` user

```` bash
adduser --gecos '' --disabled-password jenkins
````

2) Configure `ssh` to chroot

Add at the bottom of `/etc/ssh/sshd_config`

````
Match User jenkins
    ChrootDirectory /chroot/arm64/
````

3) Enable jenkins to login without password

````
mkdir /home/jenkins/.ssh
chown jenkins.jenkins /home/jenkins/.ssh
chmod 700 /home/jenkins/.ssh
cat path_to_jenkins_ssh_key >> /home/jenkins/.ssh/authorized_keys
chown jenkins.jenkins /home/jenkins/.ssh/authorized_keys
chmod 600 /home/jenkins/.ssh/authorized_keys
````

4) Create the chroot

```` bash
ARCH=arm64 ./mkchroot.sh
````

5) Mount the special folders and configure do-mounts.sh to be called at startup

```` bash
./do-mounts.sh
````

E.g. add to the bottom of `/etc/rc.local` (before exit if it is there) the following:  
`<path-to-this-folder>/do-mounts.sh`

6) Done :)

Important notes
---------------

* The ssh key is only needed on the host, not in the chroot.
* The user id for jenkins on the host and in the chroot should match.
* The chroot happens in sshd and as an unprivileged user (`jenkins`) with a low security risk. While
it is higher than running the builder in a VM, it is very close to be considered safe enough.

