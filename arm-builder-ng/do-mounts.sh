#!/bin/bash

#ARCHES="armel armhf arm64"
ARCHES="arm64"
CHROOT=/chroot

set -o pipefail

declare -A mps=(["/proc"]="proc" ["/sys"]="sysfs" ["/dev"]="devtmpfs" ["/dev/pts"]="devpts")

#echo ${!mps[@]}

for ARCH in $ARCHES; do
	# here we rely on /dev coming before /dev/pts, this will badly break if that is not the case
	for mp in "${!mps[@]}"; do
		ty="${mps[$mp]}"
		if ! cat /proc/mounts|awk '{print $2}'|grep -q "^$CHROOT/$ARCH$mp\$"; then
			echo -n $CHROOT/$ARCH$mp mounting...
			if mount -t $ty $ty $CHROOT/$ARCH$mp; then
				echo ' 'OK
			else
				echo ' 'error
			fi
		else
			echo $CHROOT/$ARCH$mp is OK
		fi
	done
done

