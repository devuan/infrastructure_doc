## files.devuan.org

server {
  listen          *:80;
  server_name     files.devuan.org vagrant.devuan.org;

  location ~ ^/.well-known/acme-challenge {
    root /home/files.devuan.org/letsencrypt;
  }

  location / {
    rewrite ^ https://files.devuan.org$uri permanent;
  }

}

server {
  listen          *:443 ssl;
  server_name     files.devuan.org;
  root            /home/files.devuan.org/public;
  index           none;

  location = /favicon.ico {
    alias /home/files.devuan.org/favicon.ico;
  }

  location ~ ^/.well-known/acme-challenge {
    root /home/files.devuan.org/letsencrypt;
  }

  location / {
    autoindex on;
    autoindex_exact_size off;     # use KB, etc.
    add_before_body /.header.html; # that's public.header.html!
    add_after_body  /.footer.html; # that's public.footer.html!
  }

  ssl                 on;
ssl_certificate /etc/letsencrypt/live/files.devuan.org/fullchain.pem; # managed by Certbot
ssl_certificate_key /etc/letsencrypt/live/files.devuan.org/privkey.pem; # managed by Certbot
  
  ssl_protocols          TLSv1.2 TLSv1.1 TLSv1;
  ssl_ciphers            "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
  ssl_ecdh_curve secp384r1; 
  ssl_prefer_server_ciphers on;
  ssl_session_cache   builtin:1000 shared:SSL:10m;
  resolver 213.186.33.99 valid=300s;
  resolver_timeout 3s;
}
